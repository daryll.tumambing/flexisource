# Flexisource

## Backend
```bash
docker-compose up
```

More details at [backend](backend)

## Frontend
```bash
cd webapp/flexisource/
npm run dev
```

More details at [frontend](frontend)


After running these 2, you should be able to access http://localhost:8080 that would communicate with the backend at http://localhost:5000