# Flexisource Backend

This is the backend of Flexisource using Flask as the main framework.

## Installation

To contribute to the code, you need to install the necessary libraries.

Create a python 3 environment and activate it.
```bash
virtualenv -ppython3 venv
source venv/bin/activate
```

Install the required libraries.
```bash
pip install -r etc/requirements.txt
```

## Testing

Ensure tests are always successful.
```bash
python -m unittest
```

## Seed

Populate the database using the seed file provided

```bash
python etc/seed/users.py
```

**After running the seed, retrieve 1 email from the printed output and use it as both the email and password in the login page.**

## Running

To run a working API, use docker-compose to setup the main backend and the database.

```bash
docker-compose up
```