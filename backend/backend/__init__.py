from flask import Flask
from flask_cors import CORS
from flask_migrate import Migrate
from backend.lib.database import db
from backend import models
import uuid

app = Flask(__name__)
CORS(app)

# Configurations
app.config["SQLALCHEMY_DATABASE_URI"] = 'postgresql://postgres:postgres@flexisourcedb:5432'
# app.config["JWT_SECRET"] = 'secret'
app.config["JWT_SECRET"] = str(uuid.uuid4())

db.init_app(app)
Migrate(app, db)

# Importing routes
import backend.api