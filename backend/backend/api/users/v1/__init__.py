from flask import json, request, jsonify, make_response
from backend import app
from backend.services import users as user_service
from backend.services import authorization
from backend.models import users as user_model
from backend.lib import logging

logger = logging.initialize(__name__)

@app.route('/api/users/v1/', methods=['GET'])
def list_users():
  if authorization.is_expired(request.headers['Authorization']):
    return make_response({'message': 'Unauthorized access'}, 403)

  authorization_payload = authorization.decode(request.headers['Authorization'])
  logger.info(authorization_payload)
  
  try:
    users = user_service.list()
  except Exception as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 500)

  return jsonify(users)

@app.route('/api/users/<user_id>/v1/', methods=['GET'])
def get_user(user_id):
  if authorization.is_expired(request.headers['Authorization']):
    return make_response({'message': 'Unauthorized access'}, 403)

  authorization_payload = authorization.decode(request.headers['Authorization'])
  logger.info(authorization_payload)
  
  try:
    user = user_service.get(user_id)
  except Exception as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 500)

  return jsonify(user)