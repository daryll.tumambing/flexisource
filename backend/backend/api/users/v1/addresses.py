from flask import json, request, jsonify, make_response
from backend import app
from backend.services import user_addresses as user_addresses_service
from backend.services import authorization
from backend.models import user_addresses as user_addresses_model
from backend.lib import logging

logger = logging.initialize(__name__)

@app.route('/api/users/v1/addresses', methods=['GET'])
def list_owner_user_addresses():
  if authorization.is_expired(request.headers['Authorization']):
    return make_response({'message': 'Unauthorized access'}, 403)

  authorization_payload = authorization.decode(request.headers['Authorization'])
  logger.info(authorization_payload)
  
  try:
    addresses = user_addresses_service.list(authorization_payload['user_id'])
  except Exception as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 500)

  return jsonify(addresses=addresses)

@app.route('/api/users/<user_id>/v1/addresses', methods=['GET'])
def list_other_user_addresses(user_id):
  if authorization.is_expired(request.headers['Authorization']):
    return make_response({'message': 'Unauthorized access'}, 403)

  authorization_payload = authorization.decode(request.headers['Authorization'])
  logger.info(authorization_payload)
  
  try:
    addresses = user_addresses_service.list(user_id)
  except Exception as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 500)

  return jsonify(addresses=addresses)

@app.route('/api/users/v1/addresses', methods=['POST'])
def create_user_address():
  if authorization.is_expired(request.headers['Authorization']):
    return make_response({'message': 'Unauthorized access'}, 403)

  authorization_payload = authorization.decode(request.headers['Authorization'])
  logger.info(authorization_payload)

  for key in user_addresses_model.REQUIRED_CREATE_FIELDS:
    if key not in request.json.keys():
      return make_response({'message': 'Missing required login fields'}, 400)

  try:
    address = user_addresses_service.create(authorization_payload['user_id'], **request.json)
  except Exception as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 500)

  return jsonify(address=address, message='Successfully added an address')