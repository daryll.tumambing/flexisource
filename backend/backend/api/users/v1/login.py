from flask import request, jsonify, make_response
from backend import app
from backend.services import users as user_service
from backend.models import users as user_model
from backend.lib import logging

logger = logging.initialize(__name__)

@app.route('/api/users/v1/login', methods=['POST'])
def login():
  if request.json == None:
    return make_response({'message': 'Missing required login fields'}, 400)

  for key in user_model.REQUIRED_LOGIN_FIELDS:
    if key not in request.json.keys():
      return make_response({'message': 'Missing required login fields'}, 400)
  
  try:
    logger.debug(str({k:v for k,v in request.json.items() if k != 'password'}) + " is trying to login")
    user_data = user_service.login(**request.json)
  except user_service.EmailTakenError as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 403)
  except user_service.InvalidEmailError as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 403)
  except user_service.InvalidPasswordError as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 403)
  except Exception as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 500)

  logger.info(str({k:v for k,v in request.json.items() if k != 'password'}) + " successfully logged in")
  return make_response({**user_data, 'message': 'Login successful!'}, 200)