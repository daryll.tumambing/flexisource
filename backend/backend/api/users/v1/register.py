from flask import request, jsonify, make_response
from backend import app
from backend.services import users as user_service
from backend.models import users as user_model
from backend.lib import logging

logger = logging.initialize(__name__)

@app.route('/api/users/v1/register', methods=['POST'])
def register():
  if request.json == None:
    return make_response({'message': 'Missing required registration fields'}, 400)

  for key in user_model.REQUIRED_REGISTRATION_FIELDS:
    if key not in request.json.keys():
      return make_response({'message': 'Missing required registration fields'}, 400)
  
  try:
    logger.debug(str({k:v for k,v in request.json.items() if k != 'password'}) + " is trying to register")
    user_service.register(**request.json)
  except Exception as e:
    logger.error(e)
    return make_response({'message': 'An error has occurred'}, 500)

  logger.info(str({k:v for k,v in request.json.items() if k != 'password'}) + " successfully registered")
  return make_response({'message': 'Registration successful!'}, 200)