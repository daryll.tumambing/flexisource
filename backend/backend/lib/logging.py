import logging

def initialize(name):
  logger = logging.getLogger(name)
  logger.setLevel(logging.DEBUG)
  logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
  # ch = logging.StreamHandler()
  # ch.setLevel(logging.DEBUG)
  # ch.setFormatter(formatter)
  # logger.addHandler(ch)
  return logger
