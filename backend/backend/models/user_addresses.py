from backend.lib.database import db
from dataclasses import dataclass

REQUIRED_CREATE_FIELDS = ['address', 'city', 'province', 'country', 'postal_code']

@dataclass
class UserAddress(db.Model):
  __tablename__ = 'user_addresses'

  id: int
  address: str
  city: str
  province: str
  country: str
  postal_code: str

  id = db.Column(db.Integer, primary_key=True)
  user_id = db.Column(db.ForeignKey("users.id"))
  user = db.relationship("User", backref=db.backref("users", uselist=False))

  address = db.Column(db.String(512), nullable=False)
  city = db.Column(db.String(128), nullable=False)
  province =db.Column(db.String(128), nullable=False)
  country = db.Column(db.String(128), unique=False, nullable=False)
  postal_code = db.Column(db.String(16), unique=False, nullable=False)

  def __init__(self, user_id, address, city, province, country, postal_code):
    self.user_id = user_id
    self.address = address
    self.city = city
    self.province = province
    self.country = country
    self.postal_code = postal_code

  def __repr__(self):
      return f"[{self.id}] {self.address} {self.city} {self.province} {self.country} {self.postal_code}"