from hashlib import sha256
import uuid
from dataclasses import dataclass
from backend.lib.database import db

REQUIRED_REGISTRATION_FIELDS = ['first_name', 'last_name', 'email', 'password']
REQUIRED_LOGIN_FIELDS = ['email', 'password']

@dataclass
class User(db.Model):
  __tablename__ = 'users'

  id: int
  first_name: str
  last_name: str
  email: str
  addresses: "UserAddress"

  id = db.Column(db.Integer, primary_key=True)
  first_name = db.Column(db.String(128), nullable=False)
  last_name = db.Column(db.String(128), nullable=False)
  email = db.Column(db.String(128), unique=True, nullable=False)
  salt = db.Column(db.String(36), nullable=False)
  password = db.Column(db.String(64), nullable=False)
  addresses = db.relationship("UserAddress", backref=db.backref("user_addresses", uselist=True))

  def __init__(self, first_name, last_name, email, password):
    self.first_name = first_name
    self.last_name = last_name
    self.email = email
    self.salt = self._generate_salt()
    self.password = self._generate_password(password, self.salt)

  def __repr__(self):
      return f"[{self.id}] {self.full_name()} <{self.email}>"

  def full_name(self):
    return f"{self.first_name} {self.last_name}"

  # Private functions

  @staticmethod
  def _generate_password(user_generated_password, salt):
    encoded_password = f"{user_generated_password}{salt}".encode()
    return sha256(encoded_password).hexdigest()

  @staticmethod
  def _generate_salt():
    return str(uuid.uuid4())