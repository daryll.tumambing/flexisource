import jwt
from datetime import datetime, timedelta
from backend import app

def is_expired(token):
  try:
    decode(token)
    return False
  except jwt.ExpiredSignatureError:
    return True

def encode(payload):
  return jwt.encode(payload, app.config["JWT_SECRET"], algorithm="HS256")

def decode(token):
   return jwt.decode(token, app.config["JWT_SECRET"], algorithms=["HS256"])