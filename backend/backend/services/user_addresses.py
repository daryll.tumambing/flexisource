from backend.models.user_addresses import UserAddress
from backend.lib.database import db
from backend import app

def create(user_id, address, city, province, country, postal_code):
  address = UserAddress(user_id, address, city, province, country, postal_code)

  db.session.add(address)
  db.session.commit()
  return address

def list(user_id):
  return UserAddress.query.where(UserAddress.user_id==user_id).all()