from flask.json import jsonify
from backend.models.users import User
from backend.lib.database import db
from backend import app
from backend.services import authorization
from datetime import datetime, timedelta

class InvalidEmailError(Exception):pass
class InvalidPasswordError(Exception):pass
class EmailTakenError(Exception):pass

def list():
  return User.query.all()

def get(user_id):
  return User.query.where(User.id==user_id).first()

def register(first_name, last_name, email, password):
  if User.query.where(User.email==email).count() == 0:
    user = User(first_name, last_name, email, password)
    db.session.add(user)
    db.session.commit()
    return user
  else:
    raise EmailTakenError("Email has already been used")

def login(email, password):
  user = User.query.where(User.email==email)
  if user.count() == 1:
    user = user.first()
    if User._generate_password(password, user.salt) == user.password:
      
      current = datetime.utcnow()
      payload = {
        "user_id": user.id,
        "exp": current + timedelta(hours=1),
        "iat": current
        }

      user_json = jsonify(user).json
      del user_json['addresses']

      return {
        "user": user_json,
        "token": authorization.encode(payload)
        }
    else:
      raise InvalidPasswordError(f"Password provided is invalid for {user}")
  else:
    raise InvalidEmailError(f"Email {email} is not existing")
  