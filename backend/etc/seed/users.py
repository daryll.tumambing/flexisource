import requests
import random
from faker import Faker
fake = Faker(['en-PH'])

populate_count = 100

s = requests.Session()
BASE_URL = 'http://localhost:5000'

for i in range(populate_count):
  fake_profile = fake.profile()
  
  registration_profile = {
    'first_name': fake_profile['name'].split()[0],
    'last_name': ' '.join(fake_profile['name'].split()[1:]),
    'email': fake_profile['mail'],
    'password': fake_profile['mail']
  }

  # Register the user
  s.post(f'{BASE_URL}/api/users/v1/register', json=registration_profile)

  credentials = {
    'email': registration_profile['email'],
    'password': registration_profile['password']
  }

  # Login using the user
  res = s.post(f'{BASE_URL}/api/users/v1/login', json=credentials)
  print(res.json())
  token = res.json()['token']

  for i in range(random.randint(3, 10)):
    address_payload = {
      "address": fake.street_address(),
      "city": fake.city(), 
      "province": fake.province(), 
      "country": fake.country(),
      "postal_code": fake.random_number()
      }
    s.post(f'{BASE_URL}/api/users/v1/addresses', json=address_payload, headers={'Authorization': token})