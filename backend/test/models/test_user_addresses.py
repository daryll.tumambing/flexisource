import unittest
from backend.models.users import User
from backend.models.user_addresses import UserAddress
from backend import app
from backend.lib.database import db


class TestUserAddressesModel(unittest.TestCase):
    def setUp(self):
      app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite://'

      self.expected_attributes = ['id', 'user_id', 'user', 'address', 'city', 'province', 'country', 'postal_code']

      with app.app_context():
        db.create_all()

        user = User('Juan', 'dela Cruz', 'jcruz@gmail.com', 'hunter2')
        db.session.add(user)
        db.session.commit()

        user_address = UserAddress(user.id, '1/F Bldg. No. 42', 'Makati City', 'NCR', 'Philippines', '1200')
        db.session.add(user_address)
        db.session.commit()

    def tearDown(self):
      with app.app_context():
        db.session.remove()
        db.drop_all()

    def test_user_addresses_model_has_correct_attributes(self):
      for attribute in self.expected_attributes:
        self.assertIn(attribute, UserAddress.__dict__.keys())
  
    def test_user_address_can_retrieve_associated_user(self):
      with app.app_context():
        self.assertEqual(User.query.first(), UserAddress.query.first().user)
    
if __name__ == '__main__':
  unittest.main()