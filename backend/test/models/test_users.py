import unittest
from backend.models.users import User

class TestUserModel(unittest.TestCase):
    def setUp(self):
        self.expected_attributes = ['id', 'first_name', 'last_name', 'email', 'password', 'salt']
        self.user = User('Juan', 'dela Cruz', 'jcruz@gmail.com', 'hunter2')

    def test_user_model_has_correct_attributes(self):
        for attribute in self.expected_attributes:
            self.assertIn(attribute, User.__dict__.keys())
  
    def test_full_name_should_contain_first_name_and_last_name(self):
        self.assertEqual(self.user.full_name(), f"{self.user.first_name} {self.user.last_name}")

    def test_user_password_should_be_salted_and_hashed(self):
        self.assertEqual(User._generate_password('hunter2', self.user.salt), self.user.password)
    
if __name__ == '__main__':
    unittest.main()