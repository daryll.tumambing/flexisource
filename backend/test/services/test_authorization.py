import unittest
from backend.services import authorization
from backend import app
from datetime import datetime, timedelta

class TestAuthorizationService(unittest.TestCase):
    def setUp(self):
      app.config["JWT_SECRET"] = 'secret'
      self.payload = {"test": True}
      self.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0ZXN0Ijp0cnVlfQ.D0gPKzCsGUyTdE9A1B4A1pBI1Ug2j7DyqN1n75W_Pdw'

    def test_is_expired(self):
      jan_1_2021 = datetime(year=2021, month=1, day=1).timestamp()
      expired_payload = {"test": True, 'exp': jan_1_2021}
      expired_token = authorization.encode(expired_payload)

      self.assertTrue(authorization.is_expired(expired_token))

    def test_encode(self):
      self.assertEqual(authorization.encode(self.payload), self.token)
    
    def test_decode(self):
      self.assertEqual(authorization.decode(self.token), self.payload)

if __name__ == '__main__':
    unittest.main()