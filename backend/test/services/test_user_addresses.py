import unittest
from backend.models.users import User
from backend import app
from backend.lib.database import db
from backend.services import user_addresses

class TestUserAddressesService(unittest.TestCase):
    def setUp(self):
      app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite://'

      with app.app_context():
        db.create_all()

        user = User('Juan', 'dela Cruz', 'jcruz@gmail.com', 'hunter2')
        db.session.add(user)
        db.session.commit()

    def tearDown(self):
      with app.app_context():
        db.session.remove()
        db.drop_all()

    def test_create(self):
      with app.app_context():
        user = User.query.first()
        address = user_addresses.create(user.id, 'address', 'city', 'province', 'country', 'postal_code')
        self.assertEqual(address.address, "address")
        self.assertEqual(address.city, "city")
        self.assertEqual(address.province, "province")
        self.assertEqual(address.country, "country")
        self.assertEqual(address.postal_code, "postal_code")

    def test_list(self):
      with app.app_context():
        user = User.query.first()
        address_count = 5
        for i in range(address_count):
          user_addresses.create(user.id, f'address{i}', f'city{i}', f'province{i}', f'country{i}', f'postal_code{i}')
        
        addresses = user_addresses.list(user.id)
        self.assertEqual(len(addresses), address_count)
    
if __name__ == '__main__':
  unittest.main()