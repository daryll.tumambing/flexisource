import unittest
from backend.models.users import User
from backend import app
from backend.lib.database import db
from backend.services import users
from backend.services.users import EmailTakenError, InvalidPasswordError, InvalidEmailError

class TestUserAddressesService(unittest.TestCase):
    def setUp(self):
      app.config["SQLALCHEMY_DATABASE_URI"] = 'sqlite://'

      with app.app_context():
        db.create_all()

    def tearDown(self):
      with app.app_context():
        db.session.remove()
        db.drop_all()

    def test_list(self):
      with app.app_context():
        user_count = 5
        for i in range(user_count):
          tmp_user = User(f'first_name{i}', f'last_name{i}', f'username{i}@email.com', f'password{i}')
          db.session.add(tmp_user)
          db.session.commit()
        
        self.assertEqual(len(users.list()), user_count)

    def test_successful_register(self):
      with app.app_context():
        user = users.register('first_name', 'last_name', 'username@email.com', 'password')
        self.assertEqual(user.first_name, 'first_name')
        self.assertEqual(user.last_name, 'last_name')
        self.assertEqual(user.email, 'username@email.com')
        self.assertNotEqual(user.password, 'password')

    def test_same_email_register(self):
      with app.app_context():
        users.register('first_name', 'last_name', 'username@email.com', 'password')
        
        with self.assertRaises(EmailTakenError):
          users.register('first_name', 'last_name', 'username@email.com', 'password')

    def test_successful_login(self):
      with app.app_context():
        users.register('first_name', 'last_name', 'username@email.com', 'password')
        user_data = users.login('username@email.com', 'password')
        header = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9'
        
        self.assertEqual(user_data['token'][:36], header)
    
    def test_invalid_email_login(self):
      with app.app_context():
        users.register('first_name', 'last_name', 'username@email.com', 'password')
    
        with self.assertRaises(InvalidEmailError):
          users.login('incorrect-username@email.com', 'password')

    def test_invalid_password_login(self):
      with app.app_context():
        users.register('first_name', 'last_name', 'username@email.com', 'password')
    
        with self.assertRaises(InvalidPasswordError):
          users.login('username@email.com', 'incorrect-password')

if __name__ == '__main__':
  unittest.main()