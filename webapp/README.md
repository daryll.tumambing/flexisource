# Flexisource Frontend

> This is the web application for this exam.

## Installation

To contribute to the code, you need to install the necessary libraries.

```bash
npm install
```

## Running

```bash
# serve with hot reload at localhost:8080
npm run dev
```

## References

Used the following references in creating a rough POC.

- https://vuejs.org/v2/guide/
- https://www.thepolyglotdeveloper.com/2018/04/simple-user-login-vuejs-web-application/
