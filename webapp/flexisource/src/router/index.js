import Vue from 'vue';
import Router from 'vue-router';
import HelloWorld from '@/components/HelloWorld';

import Profile from '@/components/profile/Profile';
import Dashboard from '@/components/dashboard/Dashboard';
import Login from '@/components/auth/Login';
import store from '../store';

Vue.use(Router);

let router = new Router({
  mode: 'history',
  // base: process.env.BASE_URL,
  routes: [
      {
          path: '/',
          name: 'dashboard',
          component: Dashboard,
          meta: {
              requiresAuth: true
          }
      },
      {
        path: '/:user_id(\\d+)',
        name: 'profile',
        component: Profile,
        meta: {
            requiresAuth: true
        }
    },
      {
          path: '/login',
          name: 'login',
          component: Login,
      },
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters['auth/getToken'] != null) {
        next()
        return
    }
    next('/login')
  } else if (to.path == '/login' && store.getters['auth/getToken'] != null) {
    next('/')
  } else {    
    next()
  }
})

export default router
