import axios from "axios";

const state = () => ({
  user_addresses: [],
  other_user_addresses: []
})

const getters = {
  getUserAddresses: (state, getters) => state.user_addresses,
  getOtherUserAddresses: (state, getters) => state.other_user_addresses
}

const actions = {
  async getUserAddresses (context) {
    await axios.get(
      `http://localhost:5000/api/users/v1/addresses`,
      {
        'headers': {'Authorization': localStorage.getItem('token')}
      }
    )
    .then(res => {
      context.commit("setUserAddresses", res.data.addresses);
    })
    .catch(error => {
      if (error['response']['status'] == 403) {
        localStorage.setItem('user', '')
        localStorage.setItem('token', '')
      }

      console.log('error caught')
    });
  },
  async getOtherUserAddresses (context, user_id) {
    await axios.get(
      `http://localhost:5000/api/users/${user_id}/v1/addresses`,
      {
        'headers': {'Authorization': localStorage.getItem('token')}
      }
    )
    .then(res => {
      context.commit("setOtherUserAddresses", res.data.addresses);
    })
    .catch(error => {
      if (error['response']['status'] == 403) {
        localStorage.setItem('user', '')
        localStorage.setItem('token', '')
      }

      console.log('error caught')
    });
  },
}

const mutations = {
  setUserAddresses(state, user_addresses) {
    state.user_addresses = user_addresses
  },
  setOtherUserAddresses(state, user_addresses) {
    state.other_user_addresses = user_addresses
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}