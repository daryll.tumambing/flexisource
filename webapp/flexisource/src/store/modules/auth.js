import axios from "axios";

const state = () => ({
  user: localStorage.getItem('user')? JSON.parse(localStorage.getItem('user')) : {},
  token: localStorage.getItem('token') || null
})

const getters = {
  getUser: (state, getters) => state.user,
  getToken: (state, getters) => state.token
}

const actions = {
  async login (context, { email, password }) {
    await axios.post(
      `http://localhost:5000/api/users/v1/login`,
      {email, password}
    )
    .then(res => {
      localStorage.setItem('token', res.data.token);
      localStorage.setItem('user', JSON.stringify(res.data.user));
      context.commit("setToken", res.data.token);
      context.commit("setUser", res.data.user);
    })
    .catch(error => {
      context.commit("setToken", null);
      context.commit("setUser", null);
      console.log('error caught')
    });
  },
  async logout (context) {
    localStorage.setItem('token', "");
    localStorage.setItem('user', "");
    context.commit("setToken", null);
    context.commit("setUser", null);
  }
}

const mutations = {
  setToken(state, token) {
    state.token = token
  },
  setUser(state, user) {
    state.user = user
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}