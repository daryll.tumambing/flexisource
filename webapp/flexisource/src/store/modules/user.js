import axios from "axios";

const state = () => ({
  users: [],
  other_user: null
})

const getters = {
  getUsers: (state, getters) => state.users,
  getOtherUser: (state, getters) => state.other_user
}

const actions = {
  async getUsers (context) {
    await axios.get(
      `http://localhost:5000/api/users/v1/`,
      {
        'headers': {'Authorization': localStorage.getItem('token')}
      }
    )
    .then(res => {
      context.commit("setUsers", res.data);
    })
    .catch(error => {
      if (error['response']['status'] == 403) {
        localStorage.setItem('user', '')
        localStorage.setItem('token', '')
      }

      console.log('error caught')
    });
  },
  async getOtherUser (context, user_id) {
    await axios.get(
      `http://localhost:5000/api/users/${user_id}/v1/`,
      {
        'headers': {'Authorization': localStorage.getItem('token')}
      }
    )
    .then(res => {
      context.commit("setOtherUser", res.data);
    })
    .catch(error => {
      if (error['response']['status'] == 403) {
        localStorage.setItem('user', '')
        localStorage.setItem('token', '')
      }

      console.log('error caught')
    });
  }
}

const mutations = {
  setUsers(state, users) {
    state.users = users
  },
  setOtherUser(state, other_user) {
    state.other_user = other_user
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}